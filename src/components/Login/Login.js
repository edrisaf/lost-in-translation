import logo from "../../assets/Logo.png";
import styles from './Login.module.css';
import {getStorage, setStorage} from "../../utils/localStorage";
import {useHistory} from 'react-router-dom';
import {useState} from "react";

function Login() {

    const history = useHistory();
    const [name, setName] = useState("");
    const [error, setError] = useState("");


    const handleChange = e => {
        setName(e.target.value);
    }

    const handleKeyPress = e => {
        // key code for "enter"
        if (e.keyCode === 13){
            handleSubmit();
        }
    }
    const handleSubmit = e => {
        e.preventDefault();
        if (name === '') {
            setError("Enter a valid username!")
        } else {
            setError("");
            setStorage('user', name);

            let user = getStorage("user");
            if (user.length > 0) {
                history.push("/translate");
            }
        }
    }

    return (
        <div>
            <div className={styles.Header}>
                <h1>Lost in Translation</h1>
            </div>
            <div className={styles.Line}/>
            <div className={styles.LoginContainer}>
                <div className={styles.logoContainer}>
                    <img src={logo} alt="logo" width="5%"/>
                </div>
                <div className={styles.logoTextContainer}>
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                </div>
            </div>
            <div className={styles.SearchBar}>
                <form className={styles.Form}>
                    <span/>
                    <input placeholder="What's your name?" onChange={handleChange} onKeyPress={handleKeyPress}/>
                    <button onClick={handleSubmit} type={"submit"}/>
                </form>
                <p>{error}</p>
                <div className={styles.purpleBar}></div>
            </div>
        </div>
    )
}

export default Login