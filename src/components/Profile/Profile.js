import styles from "./Profile.module.css";
import {useHistory} from "react-router-dom";

function Profile(){

    const history = useHistory();

    const user = localStorage.getItem("user");
    const translations = [];

    for (const [key, value] of Object.entries(localStorage)) {
        if (key === "user") continue
        translations[key] = value;
    }
    translations.reverse();

    const listTranslations = translations.map((translation) =>
    <li key={translation}>{translation}</li>
    );

    const logout = () => {
        localStorage.clear();
        history.push("/")
    }

    return (
        <div className="Profile">
            <div className={styles.Header}>
                <h1>Lost in Translation</h1>
                <button>{user}</button>
            </div>
            <div className={styles.Line}/>

            <div className={styles.Container}>
                <div className={styles.logoutContainer}>
                    <h1>Wanna log out?</h1>
                    <button onClick={logout}>Logout</button>
                </div>
                <div className={styles.lastTranslationsContainer}>
                    <h1>Translation history for: {user}</h1>
                    <ul>{listTranslations}</ul>
                </div>
            </div>
        </div>
    )
}

export default Profile