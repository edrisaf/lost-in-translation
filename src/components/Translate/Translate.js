import styles from "./Translate.module.css";
import {useState} from "react";
import {useHistory} from "react-router-dom";

function Translate(){
    const user = localStorage.getItem("user");
    const [word, setWord] = useState("");
    const [index, setIndex] = useState(1);
    const [image, setImage] = useState([]);
    const [error, setError] = useState('');

    const history = useHistory();

    const toProfile = () => {
        history.push("/profile")
    }

    const handleChange = e => {
        setWord(e.target.value);
        if (word.length > 40) {
            setError("Error: more than 40 characters entered");
        } else {
            setError("");
        }
    }

    const handleKeyPress = e => {
        // key code for "enter"
        if (e.keyCode === 13){
            translate();
        }
    }

    const translate = e => {
        e.preventDefault();
        if (word === "") setError("Enter something first")
        if ( error === "" && word !== "") {
            setIndex(index + 1)
            if (index > 10) localStorage.removeItem(String(index - 10))
            localStorage.setItem(String(index), word);
            const lcWord = word.toLowerCase();
            const letterArr = lcWord.split('');

            setImage(letterArr.map((letter, index) => {
                return <img className={'letterImg'}
                            key={index}
                            src={`./individial_signs/${letter}.png`}
                            width={60}
                            alt={letter}/>
            }));
        }
    }



    return (
        <div>
            <div className={styles.Header}>
                <h1>Lost in Translation</h1>
                <button onClick={toProfile}>{user}
                </button>
            </div>
            <div className={styles.Line}/>
            <div className={styles.SearchBar}>
                <form className={styles.Form}>
                    <span/>
                    <input placeholder="Type in text" onChange={handleChange}/>
                    <button type={"submit"} onClick={translate} onKeyPress={handleKeyPress}/>
                </form>
                <p>{error}</p>
            </div>

            <div className={styles.TranslationContainer}>
                <div className={styles.Translations}>
                    {image}
                </div>
                <div className={styles.spanContainer}>
                    <span>Translation</span>
                </div>
            </div>

        </div>
    )
}

export default Translate