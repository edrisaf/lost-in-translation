import {Redirect, Route} from 'react-router-dom';
import {getStorage} from "../utils/localStorage";

export const PublicRoute = props => {
    const user = getStorage("user");
    if (user != null) return <Redirect to={"/translate"}/>

    return <Route {...props} />
}

export default PublicRoute;
