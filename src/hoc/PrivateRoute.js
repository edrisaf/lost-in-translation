import { Redirect, Route } from 'react-router-dom'

export const PrivateRoute = props => {
    const user = localStorage.getItem('user')

    // If there is no id in localStorage, then redirect user to the start page
    if(!user) return <Redirect to={ "/" } />

    return <Route {...props} />
}

export default PrivateRoute
