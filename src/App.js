import './App.css';
import { BrowserRouter, Switch } from 'react-router-dom'

import Login from './components/Login/Login';
import Translate from './components/Translate/Translate';
import Profile from './components/Profile/Profile';
import PublicRoute from "./hoc/PublicRoute";
import PrivateRoute from "./hoc/PrivateRoute";


function App() {
  return (
      <BrowserRouter>
        <div className="App">
          <Switch>
            <PublicRoute exact path="/" component={ Login }/>
            <PrivateRoute path="/translate" component={ Translate }/>
            <PrivateRoute path="/profile" component={ Profile } />
          </Switch>
        </div>
      </BrowserRouter>
  );
}

export default App;
