# Lost in translation
A web app for translating english words and short sentences to sign language! Developed in React. To start translating, the user must log in by simply choosing a username. The user may then type in whatever they wish to translate.
